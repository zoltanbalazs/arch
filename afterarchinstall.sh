#!/bin/bash

sudo pacman -S rustup 
rustup install stable
rustup default stable

git clone https://aur.archlinux.org/paru-bin
cd paru-bin/
makepkg -si
cd ..
rm -rf paru-bin/

sudo paru -Syy

# SOFTWARE
paru -S spectacle skanlite simplescreenrecorder celluloid timeshift-bin timeshift-autosnap kitty discord teams blender

# SERVICES
paru -S bluez bluez-utils cups hplip system76-power auto-cpufreq
sudo systemctl enable --now system76-power
sudo systemctl enable --now auto-cpufreq
sudo systemctl enable --now cups.service

# FONTS
paru -S nerd-fonts-jetbrains-mono

# CODING
paru -S visual-studio-code-bin go dotnet-sdk-bin

# UTILS
paru -S exa bat zoxide fd procs starship ripgrep tealdeer grex git-delta fish yt-dlp
curl https://raw.githubusercontent.com/oh-my-fish/oh-my-fish/master/bin/install | fish

chsh -s /usr/bin/fish

tldr --update

