#!/bin/bash

paru -S timeshift-bin timeshift-autosnap zramd exa bat zoxide fd procs starship ripgrep tealdeer grex git-delta fish kitty discord teams yt-dlp

systemctl enable --now zramd.service

curl https://raw.githubusercontent.com/oh-my-fish/oh-my-fish/master/bin/install | fish
