#!/bin/bash

ln -sf /usr/share/zoneinfo/Europe/Budapest /etc/localtime
hwclock --systohc

sed -i '178s/.//' /etc/locale.gen
locale-gen

echo "LANG=en_US.UTF-8" >> /etc/locale.conf
echo "KEYMAP=hu" >> /etc/vconsole.conf

echo "arch" >> /etc/hostname

echo "127.0.0.1 localhost" >> /etc/hosts
echo "::1       localhost" >> /etc/hosts
echo "127.0.1.1 arch.localdomain    arch" >> /etc/hosts

echo root:password | chpasswd

pacman -S efibootmgr os-prober 
pacman -S sudo networkmanager network-manager-applet
pacman -S dialog mtools dosfstools base-devel
pacman -S linux-headers avahi xdg-user-dirs
pacman -S xdg-utils gvfs gvfs-smb nfs-utils
pacman -S inetutils dnsutils bluez
pacman -S bluez-utils cups hplip
pacman -S alsa-utils pipewire pipewire-alsa
pacman -S pipewire-pulse pipewire-jack pavucontrol
pacman -S pulseaudio-equalizer sof-firmware openssh
pacman -S rsync reflector acpi
pacman -S acpi_call acpid tlp
pacman -S virt-manager qemu qemu-arch-extra
pacman -S edk2-ovmf bridge-utils dnsmasq
pacman -S vde2 openbsd-netcat iptables-nft
pacman -S ipset firewalld nss-mdns
pacman -S ntfs-3g terminus-font

# pacman -S xf86-video-amdgpu # AMD Card
# pacman -S nvidia nvidia-utils nvidia-settings # NVIDIA Card

# GRUB Bootloader
# pacman -S grub
# grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=grub
# grub-mkconfig -o /boot/grub/grub.cfg

# Systemd-boot Bootloader
# bootctl --path=/boot install

systemctl enable NetworkManager
systemctl enable bluetooth
systemctl enable cups.service
systemctl enable sshd
systemctl enable avahi-daemon
systemctl enable tlp
systemctl enable reflector.timer
systemctl enable fstrim.timer
systemctl enable libvirtd
systemctl enable firewalld
systemctl enable acpid

useradd -m zoltanbalazs
echo zoltanbalazs:password | chpasswd
usermod -aG libvirt zoltanbalazs

echo "zoltanbalazs ALL=(ALL) ALL" >> /etc/sudoers.d/zoltanbalazs

printf "\e[1;32mDone! Type exit, umount -R /mnt and reboot.\e[0m\n"
