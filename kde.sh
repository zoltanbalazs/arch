#!/bin/bash

sudo timedatectl set-ntp true
sudo hwclock --systohc

git clone https://aur.archlinux.org/paru-bin
cd paru-bin/
makepkg -si
cd ..
rm -rf paru-bin/

sudo reflector -c Hungary -a 6 --sort rate --save /etc/pacman.d/mirrorlist
sudo paru -Syy

sudo firewall-cmd --add-port=1025-65535/tcp --permanent
sudo firewall-cmd --add-port=1025-65535/udp --permanent
sudo firewall-cmd --reload
# sudo virsh net-autostart default

paru --noconfirm -S system76-power
sudo systemctl enable --now system76-power
# sudo system76-power graphics integrated
paru --noconfirm -S auto-cpufreq
sudo systemctl enable --now auto-cpufreq

paru -S xorg sddm plasma spectacle skanlite simplescreenrecorder vlc papirus-icon-theme materia-kde dina-font tamsyn-font bdf-unifont ttf-bitstream-vera ttf-croscore ttf-dejavu ttf-droid gnu-free-fonts ttf-ibm-plex ttf-liberation ttf-linux-libertine noto-fonts ttf-roboto tex-gyre-fonts ttf-ubuntu-font-family ttf-anonymous-pro ttf-cascadia-code ttf-fantasque-sans-mono ttf-fira-mono ttf-hack ttf-fira-code ttf-inconsolata ttf-jetbrains-mono ttf-monofur adobe-source-code-pro-fonts cantarell-fonts inter-font ttf-opensans gentium-plus-fonts ttf-junicode adobe-source-han-sans-otc-fonts adobe-source-han-serif-otf-fonts noto-fonts-cjk noto-fonts-emoji nerd-fonts-jetbrains-mono

# paru -S kde-applications firefox obs-studio kdenlive

sudo systemctl enable sddm

printf "\e[1;32mDone! Rebooting in 5..4..3..2..1..\e[0m"
sleep 5
sudo reboot
